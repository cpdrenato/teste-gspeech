<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\StreamingRecognitionConfig;
use Google\Cloud\Speech\SpeechClient;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class AudioController extends Controller
{
    public function getData(Request $request)
    {
        //$url = Storage::download('public/track.mp3');
        // $url = asset('storage/test.flac');
        $url = asset('storage/audios/audio.raw');

        //$file = Storage::disk('public')->get('track.mp3');
        //$download = new Response($file, 200);


        // dd($request->all(), $url);
        return view('welcome')->with('downloadLink', $url);
    }

    public function transcribedText(Request $request)
    {
        $recognitionConfig = new RecognitionConfig();
        $recognitionConfig->setEncoding(AudioEncoding::FLAC);
        $recognitionConfig->setSampleRateHertz(44100);
        $recognitionConfig->setLanguageCode('en-US');
        $config = new StreamingRecognitionConfig();
        $config->setConfig($recognitionConfig);
        $file = Storage::disk('public')->get('test.flac');
        // dd($file);
        // $audioResource = fopen($file, 'r');
        $speechClient = new SpeechClient();

        $responses = $speechClient->recognizeAudioStream($config, $file);

        foreach ($responses as $element) {
            dd($element);
        }
    }
}
